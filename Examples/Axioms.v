Require Import Modal.
Require Import ModalRules.

Parameters p: P.
Parameters i: A.
Parameters g: G.

Theorem KT_test: Valid(
  K i p m-> p
).
Proof. mv.
mimp_i H.
mKT i.
mhyp H.
Qed.

Theorem K4_test: Valid(
  K i p m-> K i (K i p)
).
Proof. mv.
mimp_i H.
mK4.
mhyp H.
Qed.

Theorem K5_test: Valid(
  m~ K i p m-> K i (m~ (K i p))
).
Proof. mv.
mimp_i H.
mK5.
mhyp H.
Qed.

Theorem C4_test: Valid(
  C g p m-> C g (C g p)
).
Proof. mv.
mimp_i H.
mC4.
mhyp H.
Qed.

Theorem C5_test: Valid(
  m~ C g p m-> C g (m~ (C g p))
).
Proof. mv.
mimp_i H.
mC5.
mhyp H.
Qed.

Theorem DT_test: Valid(
  D g p m-> p
).
Proof. mv.
mimp_i H.
mDT g.
mhyp H.
Qed.

Theorem D4_test: Valid(
  D g p m-> D g (D g p)
).
Proof. mv.
mimp_i H.
mD4.
mhyp H.
Qed.

Theorem D5_test: Valid(
  m~ D g p m-> D g (m~ (D g p))
).
Proof. mv.
mimp_i H.
mD5.
mhyp H.
Qed.