Require Import Modal.
Require Import ModalRules.

Parameters p: P.
Parameters i1 i2 i3: A.

(* Some example applications of the mEK and mKE rules *)

Theorem EK_KE_test1: Valid(
  E [i1] p m<-> K i1 p
).
Proof. mv.
miff_i H1 H2.
+ mEK [i1].
  mhyp H1.
+ mKE.
  mhyp H2.
Qed.

Theorem EK_KE_Test2: Valid(
  E [i1,i2] p m<-> K i1 p m/\ K i2 p
).
Proof. mv.
miff_i H1 H2.
+ mcon_i.
  * mEK [i1, i2].
    mhyp H1.
  * mEK [i1, i2].
    mhyp H1.
+ mKE.
  * mcon_e1 (K i2 p).
    mhyp H2.
  * mcon_e2 (K i1 p).
    mhyp H2.
Qed.

Theorem EK_KE_Test3: Valid(
  E [i1,i2,i3] p m<-> K i1 p m/\ K i2 p m/\ K i3 p
).
Proof. mv.
miff_i H1 H2.
+ mcon_i.
  * mEK [i1, i2, i3].
    mhyp H1.
  * mcon_i.
    - mEK [i1, i2, i3].
      mhyp H1.
    - mEK [i1, i2, i3].
      mhyp H1.
+ mKE.
  * mcon_e1 (K i2 p m/\ K i3 p).
    mhyp H2.
  * mcon_e1 (K i3 p).
    mcon_e2 (K i1 p).
    mhyp H2.
  * mcon_e2 (K i2 p).
    mcon_e2 (K i1 p).
    mhyp H2.
Qed.
