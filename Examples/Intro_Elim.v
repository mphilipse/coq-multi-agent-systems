Require Import Modal.
Require Import ModalRules.

Parameters p q: P.
Parameters i: A.
Parameters g: G.

(* Proofs for the valid formulas on page 338 of Huth and Ryan *)

Theorem HR338_K: Valid(
    K i p m/\ K i (p m-> q)
  m->
    K i q
).
Proof. mv.
mimp_i H.
mK_i.
mimp_e p.
+ mK_e i w.
  mcon_e1 (K i (p m-> q)).
  mhyp H.
+ mK_e i w.
  mcon_e2 (K i p).
  mhyp H.
Qed.

Theorem HR338_E: Valid(
    E g p m/\ E g (p m-> q)
  m->
    E g q
).
Proof. mv.
mimp_i H.
mE_i.
mimp_e p.
+ mE_e g w.
  mcon_e1 (E g (p m-> q)).
  mhyp H.
+ mE_e g w.
  mcon_e2 (E g p).
  mhyp H.
Qed.

Theorem HR338_C: Valid(
    C g p m/\ C g (p m-> q)
  m->
    C g q
).
Proof. mv.
mimp_i H.
mC_i.
mimp_e p.
+ mC_e g w.
  mcon_e1 (C g (p m-> q)).
  mhyp H.
+ mC_e g w.
  mcon_e2 (C g p).
  mhyp H.
Qed.

Theorem HR338_D: Valid(
    D g p m/\ D g (p m-> q)
  m->
    D g q
).
Proof. mv.
mimp_i H.
mD_i.
mimp_e p.
+ mD_e g w.
  mcon_e1 (D g (p m-> q)).
  mhyp H.
+ mD_e g w.
  mcon_e2 (D g p).
  mhyp H.
Qed.

