Require Import Modal.
Require Import ModalRules.

(* Proof based on the birthday case, as discussed by Hakli and Negri *)


(* Type T for days *)
Parameter T : Type.

(* Birthday of agent a is on day n *)
Parameter B: A -> T -> P. 

(* Every agent knows their own birthday *)
Axiom KB: forall (i:A) (t:T) (w:W), (B i t w -> K i (B i t) w).


Section Birthday_Case_Simplification.

(* Some arbitrary day t *)
Parameter t: T.

(* Some arbitrary agents i1 and i2 *)
Parameters i1 i2: A.

Theorem OverlappingBirthdaysSimplified: Valid(
    B i1 t m/\ B i2 t
  m->
    D [i1, i2] (B i1 t m/\ B i2 t)
).
Proof. mv.
mimp_i H.
mD_i.
mcon_i.
+ mD_e [i1, i2] w.
  mKD i1.
  apply KB.
  mcon_e1 (B i2 t).
  mhyp H.
+ mD_e [i1, i2] w.
  mKD i2.
  apply KB.
  mcon_e2 (B i1 t).
  mhyp H.
Qed.

Reset Birthday_Case_Simplification.


(* Some arbitrary group g *)
Parameter g: G.

(* Lifted proposition for group membership *)
Definition min (g:G) (i:A) (w:W) := g i.

Lemma BirthdaysDoOverlap: Valid(
    (mexists i1, mexists i2, mexists t, min g i1 m/\ min g i2 m/\ B i1 t m/\ B i2 t)
  m->
    D g (mexists i1, mexists i2, mexists t, min g i1 m/\ min g i2 m/\ B i1 t m/\ B i2 t)
).
Proof. mv.
mimp_i H.
mexi_e (mexists i1 : A, mexists i2 : A, mexists t : T, min g i1 m/\ min g i2 m/\ B i1 t m/\ B i2 t) w i1 H1.
mhyp H.
mexi_e (mexists i2 : A, mexists t : T, min g i1 m/\ min g i2 m/\ B i1 t m/\ B i2 t) w i2 H2.
mhyp H1.
mexi_e (mexists t : T, min g i1 m/\ min g i2 m/\ B i1 t m/\ B i2 t) w t H3.
mhyp H2.
destruct H3. (* We use some default Coq tactics for convenience *)
destruct H3.
destruct H4.
mD_i.
mexi_i i1.
mexi_i i2.
mexi_i t.
mcon_i.
mhyp H0.
mcon_i.
mhyp H3.
mcon_i.
+ mD_e g w.
  mKD i1.
  mhyp H0.
  apply KB.
  mhyp H4.
+ mD_e g w.
  mKD i2.
  mhyp H3.
  apply KB.
  mhyp H5.
Qed.

(* Every agent has a birthday *)
Axiom atLeastOneBirthday: forall (i:A) (w:W), exists (t:T), B i t w.

(* Every agent has at most one birthday *)
Axiom atMostOneBirthday: forall (i:A) (t1 t2:T) (w:W), ((B i t1 m/\ B i t2) w) -> t1 = t2.

Lemma BirthdaysDoNotOverlap: Valid(
    m~ (mexists i1, mexists i2, mexists t, min g i1 m/\ min g i2 m/\ B i1 t m/\ B i2 t)
  m->
    D g (m~ (mexists i1, mexists i2, mexists t, min g i1 m/\ min g i2 m/\ B i1 t m/\ B i2 t))
).
Proof. mv.
mimp_i H.

mimp_e (
  mforall t1, mforall t2, mforall i1, mforall i2,
    min g i1 m/\ min g i2 m/\ B i1 t1 m/\ B i2 t2 m-> m~ (t1 m=t2)
).
+ mall_i t1.
  mall_i t2.
  mall_i i1.
  mall_i i2.
  mimp_i H0.
  destruct H0.
  destruct H1.
  destruct H2.
  mneg_i (
    mexists i1 : A, mexists i2 : A, mexists t : T, min g i1 m/\ min g i2 m/\ B i1 t m/\ B i2 t
  ) w H4.
  mhyp H.
  mexi_i i1.
  mexi_i i2.
  mexi_i t1.
  mcon_i.
  mhyp H0.
  mcon_i.
  mhyp H1.
  mcon_i.
  mhyp H2.
  replace t1 with t2.
  mhyp H3.
+ mimp_i H0.
  mD_i.
  mneg_i (mforall t1 : T, mforall t2 : T, mforall i1 : A, mforall i2 : A,
            min g i1 m/\ min g i2 m/\ B i1 t1 m/\ B i2 t2 m-> m~ (t1 m= t2)) w N1.
  * mexi_e (mexists i1, mexists i2, mexists t, min g i1 m/\ min g i2 m/\ B i1 t m/\ B i2 t) w0 i1 N2.
    mhyp N1.
    mexi_e (mexists i2, mexists t, min g i1 m/\ min g i2 m/\ B i1 t m/\ B i2 t) w0 i2 N3.
    mhyp N2.
    mexi_e (mexists t, min g i1 m/\ min g i2 m/\ B i1 t m/\ B i2 t) w0 t N4.
    mhyp N3.
    destruct N4.
    destruct H2.
    destruct H3.

    mimp_e (mexists t1, B i1 t1).
    apply atLeastOneBirthday.
    mimp_i B1.
    mexi_e (mexists t1 : T, B i1 t1) w t1 B1a.
    mhyp B1.

    mimp_e (mexists t2, B i2 t2).
    apply atLeastOneBirthday.
    mimp_i B2.
    mexi_e (mexists t2 : T, B i2 t2) w t2 B2a.
    mhyp B2.

    mneg_e (t1 m= t2) w.
    - mimp_e (min g i1 m/\ min g i2 m/\ B i1 t1 m/\ B i2 t2).
      ++ mcon_i.
         mhyp H1.
         mcon_i.
         mhyp H2.
         mcon_i.
         mhyp B1a.
         mhyp B2a.
      ++ mall_e (mforall i2 : A,
                 min g i1 m/\ min g i2 m/\ B i1 t1 m/\ B i2 t2 m-> m~ (t1 m= t2)) w i2.
         mall_e (mforall i1 : A, mforall i2 : A,
                 min g i1 m/\ min g i2 m/\ B i1 t1 m/\ B i2 t2 m-> m~ (t1 m= t2)) w i1.
         mall_e (mforall t2 : T, mforall i1 : A, mforall i2 : A,
                 min g i1 m/\ min g i2 m/\ B i1 t1 m/\ B i2 t2 m-> m~ (t1 m= t2)) w t2.
         mall_e (mforall t1 : T, mforall t2 : T, mforall i1 : A, mforall i2 : A,
                 min g i1 m/\ min g i2 m/\ B i1 t1 m/\ B i2 t2 m-> m~ (t1 m= t2)) w t1.
         mhyp H0.
    - mimp_e (min g i1). (* Convert knowledge of group membership i1 and i2 to world w *)
      mhyp H1.
      mimp_i H5.
      mimp_e (min g i2).
      mhyp H2.
      mimp_i H6.
      replace t1 with t.
      replace t2 with t.
      ++ reflexivity.
      ++ apply atMostOneBirthday with i2 w0.
         mcon_i.
         mhyp H4.
         mD_e g w.
         mKD i2.
         mhyp H6.
         apply KB.
         mhyp B2a.
      ++ apply atMostOneBirthday with i1 w0.
         mcon_i.
         mhyp H3.
         mD_e g w.
         mKD i1.
         mhyp H5.
         apply KB.
         mhyp B1a.
 * mhyp H0.
Qed.

Lemma LEM: forall (p:P) (w:W), (p m\/ m~ p) w.
Proof.
intros p w.
mneg_e' (m~ (p m\/ m~ p)) w H.
+ mneg_i (p m\/ m~ p) w H0.
  mhyp H0.
  mdis_i2.
  mneg_i (p m\/ m~ p) w H1.
  mhyp H0.
  mdis_i1.
  mhyp H1.
+ mhyp H.
Qed.

Theorem OverlappingBirthdaysFull: Valid(
    D g (mexists i1, mexists i2, mexists t, min g i1 m/\ min g i2 m/\ B i1 t m/\ B i2 t)
  m\/
    D g (m~ (mexists i1, mexists i2, mexists t, min g i1 m/\ min g i2 m/\ B i1 t m/\ B i2 t))
).
Proof. mv.
mimp_e (
    (mexists i1, mexists i2, mexists t, min g i1 m/\ min g i2 m/\ B i1 t m/\ B i2 t)
  m\/
    (m~ (mexists i1, mexists i2, mexists t, min g i1 m/\ min g i2 m/\ B i1 t m/\ B i2 t))
).
+ apply LEM.
+ mimp_i H.
  mdis_e (
      (mexists i1, mexists i2, mexists t, min g i1 m/\ min g i2 m/\ B i1 t m/\ B i2 t)
    m\/
      (m~ (mexists i1, mexists i2, mexists t, min g i1 m/\ min g i2 m/\ B i1 t m/\ B i2 t))
  ) w C1 C2.
  * mhyp H.
  * mdis_i1.
    apply BirthdaysDoOverlap.
    mhyp C1.
  * mdis_i2.
    apply BirthdaysDoNotOverlap.
    mhyp C2.
Qed.
