Require Import Modal.
Require Import ModalRules.

Parameters p q r: P.
Parameters i1 i2 i3 i4: A.

(* Some example applications of the mKD rule *)

Theorem example2_2: Valid(
    K i1 p m/\ K i2 (p m-> q)
  m->
    D [i1, i2] q
).
Proof. mv.
mimp_i H.
mD_i.
mimp_e p.
+ mD_e [i1, i2] w.
  mKD i1.
  mcon_e1 (K i2 (p m-> q)).
  mhyp H.
+ mD_e [i1, i2] w.
  mKD i2.
  mcon_e2 (K i1 p).
  mhyp H.
Qed.

Theorem Hakli_Negri_example: Valid(
    K i1 p m/\ K i2 (p m-> q) m/\ K i3 (p m/\ q m-> r)
  m->
    D [i1, i2, i3] r
).
Proof. mv.
mimp_i H.
mD_i.
mimp_e (p m/\ q).
+ mcon_i.
  * mD_e [i1, i2, i3] w.
    mKD i1.
    mcon_e1 (K i2 (p m-> q) m/\ K i3 (p m/\ q m-> r)).
    mhyp H.
  * mimp_e p.
    - mD_e [i1, i2, i3] w.
      mKD i1.
      mcon_e1 (K i2 (p m-> q) m/\ K i3 (p m/\ q m-> r)).
      mhyp H.
    - mD_e [i1, i2, i3] w.
      mKD i2.
      mcon_e1 (K i3 (p m/\ q m-> r)).
      mcon_e2 (K i1 p).
      mhyp H.
+ mD_e [i1, i2, i3] w.
  mKD i3.
  mcon_e2 (K i2 (p m-> q)).
  mcon_e2 (K i1 p).
  mhyp H.
Qed.

Theorem ED_test: Valid(
    E [i1, i2] p m/\ E [i3, i4] (p m-> q)
  m->
    D [i1, i3] q
).
Proof. mv.
mimp_i H.
mD_i.
mimp_e p.
+ mD_e [i1, i3] w.
  mKD i1.
  mEK [i1, i2].
  mcon_e1 (E [i3, i4] (p m-> q)).
  mhyp H.
+ mD_e [i1, i3] w.
  mKD i3.
  mEK [i3, i4].
  mcon_e2 (E [i1, i2] p).
  mhyp H.
Qed.
