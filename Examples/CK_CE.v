Require Import Modal.
Require Import ModalRules.

Parameters p: P.
Parameters i1 i2 i3 i4: A.
Parameters g: G.

(* Some example applications of the mCK and mCE rules *)

Theorem CK_test: Valid(
  C [i1, i2, i3, i4] p m-> K i1 p m/\ K i1 (K i2 p) m/\ K i1 (K i2 (K i3 p)) m/\ K i1 (K i2 (K i3 (K i4 p)))
).
Proof. mv.
mimp_i H.
mcon_i.
+ (* n=0 *)
  mCK [i1, i2, i3, i4].
  mhyp H.
+ mcon_i.

  * (* n=1 *)
    mCK [i1, i2, i3, i4].
    mhyp H.
  * mcon_i.

    - (* n=2 *)
      mCK [i1, i2, i3, i4].
      mhyp H.

    - (* n=3 *)
      mCK [i1, i2, i3, i4].
      mhyp H.
Qed.

Theorem CK_test1: g i1 -> g i2 -> g i3 -> g i4 ->
Valid(
  C g p m-> K i1 p m/\ K i1 (K i2 p) m/\ K i1 (K i2 (K i3 p)) m/\ K i1 (K i2 (K i3 (K i4 p)))
).
Proof.
intros H1 H2 H3 H4.
mv.
mimp_i H.
mcon_i.
+ (* n=0 *)
  mCK g. (* Here g i1 appears as subgoal *)
  mhyp H.
  mhyp H1.
+ mcon_i.

  * (* n=1 *)
    mCK g.
    mhyp H.
    mhyp H1.
    mhyp H2.
  * mcon_i.

    - (* n=2 *)
      mCK g.
      mhyp H.
      mhyp H1.
      mhyp H2.
      mhyp H3.

    - (* n=3 *)
      mCK g.
      mhyp H.
      mhyp H1.
      mhyp H2.
      mhyp H3.
      mhyp H4.
Qed.

Theorem CE_test: Valid(
  C g p m-> E g p m/\ E g (E g p) m/\ E g (E g (E g p)) m/\ E g (E g (E g (E g p)))
).
Proof. mv.
mimp_i H.
mcon_i.

+ (* n=0 *)
  mCE.
  mhyp H.
+ mcon_i.

  * (* n=1 *)
    mCE.
    mhyp H.
  * mcon_i.

    - (* n=2 *)
      mCE.
      mhyp H.

    - (* n=3 *)
      mCE.
      mhyp H.
Qed.