Require Import Modal.
Require Import ModalRules.

(* 
   Proof of the Three Wise Men puzzle

   Based on Huth and Ryan, and the course material of Logic and Applications by Engelbert Hubbers
*)

Parameters i1 i2 i3: A.

(* Agent a wears a red hat and not a white hat*)
Parameter p: A -> P. 

(* Assumptions about the hats *)
Axiom OneMustBeRed: Valid(C [i1, i2, i3] (p i1 m\/ p i2 m\/ p i3)).
Axiom OthersKnowRed1a: Valid(C [i1, i2, i3] (p i1 m-> K i2 (p i1))).
Axiom OthersKnowRed1b: Valid(C [i1, i2, i3] (p i1 m-> K i3 (p i1))).
Axiom OthersKnowRed2a: Valid(C [i1, i2, i3] (p i2 m-> K i1 (p i2))).
Axiom OthersKnowRed2b: Valid(C [i1, i2, i3] (p i2 m-> K i3 (p i2))).
Axiom OthersKnowRed3a: Valid(C [i1, i2, i3] (p i3 m-> K i1 (p i3))).
Axiom OthersKnowRed3b: Valid(C [i1, i2, i3] (p i3 m-> K i2 (p i3))).
Axiom OthersKnowWhite1a: Valid(C [i1, i2, i3] (m~ p i1 m-> K i2 (m~ p i1))).
Axiom OthersKnowWhite1b: Valid(C [i1, i2, i3] (m~ p i1 m-> K i3 (m~ p i1))).
Axiom OthersKnowWhite2a: Valid(C [i1, i2, i3] (m~ p i2 m-> K i1 (m~ p i2))).
Axiom OthersKnowWhite2b: Valid(C [i1, i2, i3] (m~ p i2 m-> K i3 (m~ p i2))).
Axiom OthersKnowWhite3a: Valid(C [i1, i2, i3] (m~ p i3 m-> K i1 (m~ p i3))).
Axiom OthersKnowWhite3b: Valid(C [i1, i2, i3] (m~ p i3 m-> K i2 (m~ p i3))).

Lemma L1: forall (p1 p2:P), Valid(
    m~ (m~ p1 m/\ m~ p2)
  m->
    p1 m\/ p2
).
Proof.
intros p1 p2 w.
mimp_i H.
mneg_e' (m~ p1 m/\ m~ p2) w H0.
* mhyp H.
* mcon_i.
  + mneg_i (p1 m\/ p2) w H1.
    - mhyp H0.
    - mdis_i1.
      mhyp H1.
  + mneg_i (p1 m\/ p2) w H1.
    - mhyp H0.
    - mdis_i2.
      mhyp H1.
Qed.

Lemma L2: forall (p1 p2 p3:P) (w:W),
    (m~ p2 m/\ m~ p3) w
  ->
    (p1 m\/ p2 m\/ p3) w
  ->
    p1 w
.
Proof.
intros p1 p2 p3 w H H0.
mdis_e (p1 m\/ p2 m\/ p3) w H1 H1.
* mhyp H0.
* mhyp H1.
* mdis_e (p2 m\/ p3) w H2 H2.
  + mhyp H1.
  + mneg_e p2 w.
    - mcon_e1 (m~ p3).
      mhyp H.
    -  mhyp H2.
  + mneg_e p3 w.
    - mcon_e2 (m~ p2).
      mhyp H.
    -  mhyp H2.
Qed.

Theorem Step1: Valid(
    C [i1, i2, i3] (m~ K i1 (p i1) m/\ m~ K i1 (m~ p i1))
  m->
    C [i1, i2, i3] (p i2 m\/ p i3)
).
Proof. mv.
mimp_i H.
mC_i.
apply L1.
mneg_i (K i1 (p i1)) w0 H0.
* mcon_e1 (m~ K i1 (m~ p i1)).
  mC_e [i1, i2, i3] w.
  mhyp H.
* mK_i.
  apply L2 with (p2:=p i2) (p3:= p i3).
  + mcon_i.
    - mK_e i1 w0.
      mimp_e (m~ p i2).
      ++ mcon_e1 (m~ p i3).
         mhyp H0.
      ++ mC_e [i1, i2, i3] w.
         apply OthersKnowWhite2a.
    - mK_e i1 w0.
      mimp_e (m~ p i3).
      ++ mcon_e2 (m~ p i2).
         mhyp H0.
      ++ mC_e [i1, i2, i3] w.
         apply OthersKnowWhite3a.
  + mK_e i1 w0.
    mCK [i1, i2, i3].
    apply OneMustBeRed.
Qed.

Lemma L3: forall (p1 p2:P) (w:W),
    (m~ p2) w
  ->
    (p1 m\/ p2) w
  ->
    p1 w
.
Proof.
intros p1 p2 w H1 H2.
mdis_e (p1 m\/ p2) w H3 H3.
* mhyp H2.
* mhyp H3.
* mneg_e p2 w.
  + mhyp H1.
  + mhyp H3.
Qed.

Theorem Step2: Valid(
    C [i1, i2, i3] (p i2 m\/ p i3)
  m->
    C [i1, i2, i3] (m~ K i2 (p i2) m/\ m~ K i2 (m~ p i2))
  m->
    K i3 (p i3)
).
Proof. mv.
mimp_i H.
mimp_i H0.
mK_i.
mneg_e' (K i2 (p i2)) w0 H1.
* mcon_e1 (m~ K i2 (m~ p i2)).
  mK_e i3 w.
  mCK [i1, i2, i3].
  mhyp H0.
* mK_i.
  apply L3 with (p2:=p i3).
  + mK_e i2 w0.
    mimp_e (m~ p i3).
    - mhyp H1.
    - mK_e i3 w.
      mCK [i1, i2, i3].
      apply OthersKnowWhite3b.
  + mK_e i2 w0.
    mK_e i3 w.
    mCK [i1, i2, i3].
    mhyp H.
Qed.

(* The puzzle would be much easier if the agents would just share their knowledge ;) *)

Theorem JustUseDistributedKnowledge: Valid (
    (p i3 m-> D [i1, i2, i3] (p i3))
  m/\
    (m~ p i3 m-> D [i1, i2, i3] (m~ p i3))
).
Proof. mv.
mcon_i.
* mimp_i H.
  mKD i1.
  mimp_e (p i3).
  + mhyp H.
  + mKT i1.
    mCK [i1, i2, i3].
    apply OthersKnowRed3a.
* mimp_i H.
  mKD i1.
  mimp_e (m~ p i3).
  + mhyp H.
  + mKT i1.
    mCK [i1, i2, i3].
    apply OthersKnowWhite3a.
Qed.
