default:
	# Compile implementation
	coqc -topfile ./Modal.v './Modal.v'
	coqc -topfile ./ModalRules.v './ModalRules.v'

start: default
	coqide ./Examples/*
	make clean

clean:
	# Clean up compiled files
	rm *.glob
	rm *.vo
	rm *.vok
	rm *.vos
