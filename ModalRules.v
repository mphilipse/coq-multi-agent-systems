(*
    Natural deduction proof rules for Modal.v

    Based on ProofWeb.v
*)

Require Import Modal.
Require Import Classical.

Ltac mcon_i :=
  match goal with
  | |- (_ m/\ _) _ => split
  end ||
  fail "(the goal is not a conjunction)".

Lemma mcone1:
forall right left w,
  (left m/\ right) w -> left w.
Proof.
unfold "m/\".
intros r l w H.
destruct H.
assumption.
Qed.

Ltac mcon_e1 R := apply (mcone1 R).

Lemma mcone2:
forall left right w,
  (left m/\ right) w -> right w.
Proof.
unfold "m/\".
intros r l w H.
destruct H.
assumption.
Qed.

Ltac mcon_e2 L := apply (mcone2 L).

Ltac mdis_i1 :=
  match goal with
  | |- (_ m\/ _) _ => left
  end ||
  fail "(the goal is not a disjunction)".

Ltac mdis_i2 :=
  match goal with
  | |- (_ m\/ _) _ => right
  end ||
  fail "(the goal is not a disjunction)".

Ltac mdis_e X w H1 H2 :=
  match X with
  | (_ m\/ _) =>
    let x := fresh "H" in
    assert (x : (X w));
    [ idtac | elim x; clear x; [intro H1 | intro H2] ]
  end ||
  fail "(the argument is not a disjunction or the labels already exist)".

Ltac mimp_i X :=
  match goal with
  | |- (_ m-> _) _ => intro X
  end ||
  fail "(the goal is not an implication)".

Lemma mimpe:
forall (prem conc:P) (w:W),
  (prem w) -> ((prem m-> conc) w) -> (conc w).
Proof.
unfold "m->".
intros p c w H H0.
apply H0.
assumption.
Qed.

Ltac mimp_e X := apply (mimpe X).

Ltac miff_i H1 H2 :=
 match goal with
 | |- (_ m<-> _) _ =>
 split; [ (intro H1) | (intro H2) ]
 end ||
 fail "(the goal is not a bi-implication)".

Lemma miffe1:
forall (L R:P) (w:W),
  (L w) -> ((L m<->R) w) -> R w.
Proof.
intros L R w H H0.
apply H0.
exact H.
Qed.

Ltac miff_e1 L := apply miffe1 with L.

Lemma miffe2:
forall (L R:P) (w:W),
  (R w) -> ((L m<->R) w) -> L w.
Proof.
intros L R w H H0.
apply H0.
exact H.
Qed.

Ltac miff_e2 R := apply miffe2 with R.

Ltac mall_i X :=
  match goal with
  | |- (mforall _, _) _ => intro X
  end ||
  fail "(the goal is not a universal quantification)".

Ltac mall_e X w A :=
  match X with
  | (mforall _ : _ , _) => refine ((_ : X w) A)
  end ||
  fail "(the argument is not a universal quantification)".

Ltac mexi_i X :=
  match goal with
  | |- (mexists x : _ , _) _ => exists X
  end ||
  fail "(the goal is not an existential quantification)".

Ltac mexi_e X w a H :=
  match X with
  | (mexists x : _ , _) => refine ((fun x y => ex_ind y (x : X w)) _ _); [idtac | intros a H]
  end ||
  fail "(the argument is not an existential quantification)".

Ltac mabsurd X w := absurd (X w); [
    match goal with
      | |- ~ (?p') ?w' => fold ((m~ p') w')
    end
  |].

Ltac mneg_i X w Y :=
  match goal with
  | |- (m~ _) _ => intro Y; mabsurd X w
  end ||
  fail "(the goal is not a negation)".

Ltac mneg_e X w := mabsurd X w.

Ltac mnegneg_e :=
  match goal with
  | |- ?H => apply (NNPP H)
  end.

Ltac mneg_e' X w Y := mnegneg_e; intro Y; mabsurd X w.

Ltac mhyp X := exact X.
