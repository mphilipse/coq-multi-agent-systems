(*
    Multi Agent Systems - S5^n Coq Implementation

    Special thanks to Engelbert Hubbers and Freek Wiedijk

    Based on Modal Logic implementation by Bruno Woltzenlogel Paleo and Christoph Benzmueller
*)


Parameter W: Type. (* Type for worlds *)
Parameter A: Type. (* Type for agents *)

Definition P := W -> Prop. (* Type of lifted propositions *)

Parameter R: A -> W -> W -> Prop. (* Accessibility relation *)

Definition G := A -> Prop. (* Groups of agents *) 
(* Return True iff the agent is in the group *)


(* Notation of sets of agents, based on ListNotation *)
Notation "[ ]" := (fun i:A => False) (format "[ ]").
Notation "[ i1 ]" := (fun i:A => i=i1).
Notation "[ i1 , i2 , .. , iN ]" := (fun i:A => (i=i1 \/ (i=i2 \/ .. (i=iN \/ False) ..))).

Notation "'gforall'  i : g , p" := (forall i : A, g i -> p)
  (at level 200, i ident, right associativity) : type_scope.
Notation "'gexists'  i : g , p" := (exists i : A, g i /\ p)
  (at level 200, i ident, right associativity) : type_scope.

(* Tactic for proving that an agent i is in group g *)
Ltac minG :=
  match goal with
  | |- _ \/ _ => (left; reflexivity) || (right; minG)
  | |- _ = _  => reflexivity
  end ||
  fail "(could not find equal element in disjunction)".


(* Modal connectives *)
Definition mequal {A: Type}(x y: A)(w:W) := x = y.
Notation "x m= y" := (mequal x y) (at level 99, right associativity).

Definition mnot (p:P)(w:W) := ~ (p w).
Notation "m~ p" := (mnot p) (at level 74, right associativity).

Definition mand (p q:P)(w:W) := (p w) /\ (q w).
Notation "p m/\ q" := (mand p q) (at level 79, right associativity).

Definition mor (p q:P)(w:W) := (p w) \/ (q w).
Notation "p m\/ q" := (mor p q) (at level 79, right associativity).

Definition mimplies (p q:P)(w:W) := (p w) -> (q w).
Notation "p m-> q" := (mimplies p q) (at level 99, right associativity).

Definition mequiv (p q:P)(w:W) := (p w) <-> (q w).
Notation "p m<-> q" := (mequiv p q) (at level 99, right associativity).


(* Modal quantifiers *)
Definition mA {t:Type}(p:t -> P)(w:W) := forall x, p x w.
Notation "'mforall'  x , p" := (mA (fun x => p))
  (at level 200, x ident, right associativity) : type_scope.
Notation "'mforall' x : t , p" := (mA (fun x:t => p))
  (at level 200, x ident, right associativity, 
    format "'[' 'mforall' '/ '  x  :  t , '/ '  p ']'")
  : type_scope.

Definition mE {t:Type}(p:t -> P)(w:W) := exists x, p x w.
Notation "'mexists' x , p" := (mE (fun x => p))
  (at level 200, x ident, right associativity) : type_scope.
Notation "'mexists' x : t , p" := (mE (fun x:t => p))
  (at level 200, x ident, right associativity, 
    format "'[' 'mexists' '/ '  x  :  t , '/ '  p ']'")
  : type_scope.


(* Knowledge operators *)
Definition K (i:A) (p:P) := fun x => forall y, (R i x y) -> (p y).
Definition E (g:G) (p:P) := fun x => gforall i : g, K i p x.
Fixpoint E' (n:nat) (g:G) (p:P) :=
  match n with
  | O => E g p
  | S n' => E' n' g (E g p)
end.
Definition C (g:G) (p:P) := fun x => forall n : nat, E' n g p x.
Definition D (g:G) (p:P) := fun x => forall y, (gforall i : g, R i x y) -> p y.


(* Modal validity of lifted propositions *)
Definition Valid (p:P) := forall w : W, p w.
Ltac mv := match goal with [|- (Valid _)] => intro end.


(* Accessibility relations R_E, R_D, and R_C (Huth & Ryan, page 338) *)
Definition RE (g:G) (x y:W) := gexists i : g, R i x y.
Fixpoint RE' (n:nat) (g:G) (x y:W) :=
  match n with
  | O => RE g x y
  | S n' => exists z:W, RE' n' g x z /\ RE g z y
end.
Definition RC (g:G) (x y:W) := exists n : nat, RE' n g x y.
Definition RD (g:G) (x y:W) := gforall i : g, R i x y.

(* RE accurately describes E *)
Lemma REE: forall g p x, (E g p x) <-> (forall y, (RE g x y) -> (p y)).
Proof.
intros.
unfold RE.
unfold E.
unfold K.
split.
+ intros.
  destruct H0 as [i H0].
  destruct H0.
  cut (R i x y).
  apply H.
  exact H0.
  exact H1.
+ intros.
  apply H.
  exists i.
  split.
  exact H0.
  exact H1.
Qed.

(* RE' accurately describes E' *)
Lemma RE'E': forall n g x p, (E' n g p x) <-> (forall y, (RE' n g x y) -> (p y)).
Proof.
intros n g x.
induction n.
+ split.
  * intros.
    unfold E' in H.
    unfold RE' in H0.
    cut (RE g x y).
    generalize y.
    apply REE.
    exact H.
    exact H0.
  * simpl.
    apply REE.
+ split.
  * simpl.
    intros.
    destruct H0 as [z H0].
    destruct H0.
    generalize (IHn (E g p)).
    intros.
    destruct H2.
    apply (proj1 (REE g p z)); auto.
  * simpl.
    intros.
    generalize (IHn (E g p)).
    intros.
    apply H0.
    intros.
    apply (REE g p y).
    intros z H2.
    apply H.
    exists y.
    split.
    exact H1.
    exact H2.
Qed.

(* RC accurately describes C *)
Lemma RCC: forall g x p, (C g p x) <-> (forall y, (RC g x y) -> (p y)).
Proof.
split.
+ intros H y H0.
  unfold RC in H0.
  destruct H0 as [n H0].
  cut (RE' n g x y).
  generalize y.
  apply RE'E'.
  unfold C in H.
  generalize n.
  exact H.
  exact H0.
+ intro H.
  unfold C.
  intro n.
  apply RE'E'.
  intros y H0.
  apply H.
  unfold RC.
  exists n.
  exact H0.
Qed.

(* RD accurately describes D *)
Lemma RDD: forall g p x, (D g p x) <-> (forall y, (RD g x y) -> (p y)).
Proof.
unfold D.
unfold RD.
split.
+ intros H y H0.
  apply H.
  exact H0.
+ intros H y H0.
  apply H.
  exact H0.
Qed.


(* Lemmas for proof tactics *)
Lemma Ke_lemma:
forall (i:A) (x:W) (p:P) (y:W),
  (K i p x) -> (R i x y) -> p y.
Proof.
intros i x p y H.
generalize y.
exact H.
Qed.

Lemma Ei_lemma:
forall (g:G) (x:W) (p:P),
  (forall y, (RE g x y) -> p y) -> (E g p x).
Proof.
intros g x p H.
apply REE.
exact H.
Qed.

Lemma Ee_lemma:
forall (g:G) (x:W) (p:P) (y:W),
  (E g p x) -> (RE g x y) -> p y.
Proof.
intros g x p y H.
generalize y.
apply (REE g p x).
exact H.
Qed.

Lemma Ci_lemma:
forall (g:G) (x:W) (p:P),
  (forall y, (RC g x y) -> p y) -> (C g p x).
Proof.
intros g x p H.
apply (RCC g x p).
exact H.
Qed.

Lemma Ce_lemma:
forall (g:G) (x:W) (p:P) (y:W),
  (C g p x) -> (RC g x y) -> p y.
Proof.
intros g x p y H.
generalize y.
apply (RCC g x p).
exact H.
Qed.

Lemma Di_lemma:
forall (g:G) (x:W) (p:P),
  (forall y, (RD g x y) -> p y) -> (D g p x).
Proof.
intros g x p H.
apply (RDD g p x).
exact H.
Qed.

Lemma De_lemma:
forall (g:G) (x:W) (p:P) (y:W),
  (D g p x) -> (RD g x y) -> p y.
Proof.
intros g x p y H.
generalize y.
apply (RDD g p x).
exact H.
Qed.

Lemma KE_lemma:
forall (g:G) (p:P) (x:W),
  (gforall i:g, K i p x) -> E g p x.
Proof.
intros g p x H.
exact H.
Qed.

Lemma EK_lemma:
forall (g:G) (p:P) (x:W),
  E g p x -> gforall i:g, K i p x.
Proof.
intros g p x H.
exact H.
Qed.

Lemma CE_lemma:
forall (g:G) (p:P) (x:W) (n:nat),
  C g p x -> E' n g p x.
Proof.
intros g p x n H.
generalize n.
exact H.
Qed.

Lemma KD_lemma:
forall (i:A) (g:G) (p:P) (x:W),
  g i -> K i p x -> D g p x.
Proof.
intros i g p x H H0.
unfold D.
intros y H1.
apply H0.
apply H1.
exact H.
Qed.


(* Convenient tactics for knowledge operators *)
Ltac mK_i := let w := fresh "w" in let R := fresh "R0"
             in (intro w at top; intro R).

Ltac mK_e i w := match goal with
  | |- _ ?w0 => apply (Ke_lemma i w); [> clear dependent w0 | try assumption]
end.

Ltac mE_i := let w := fresh "w" in let R := fresh "RE0"
             in (apply Ei_lemma; intro w at top; intro R).

Ltac mE_e g w := match goal with
  | |- _ ?w0 => apply (Ee_lemma g w); [> clear dependent w0 | try assumption]
end.

Ltac mC_i := let w := fresh "w" in let R := fresh "RC0"
             in (apply Ci_lemma; intro w at top; intro R).

Ltac mC_e g w := match goal with
  | |- _ ?w0 => apply (Ce_lemma g w); [> clear dependent w0 | try assumption]
end.

Ltac mD_i := let w := fresh "w" in let R := fresh "RD0"
             in (apply Di_lemma; intro w at top; intro R).

Ltac mD_e g w := match goal with
  | |- _ ?w0 => apply (De_lemma g w); [> clear dependent w0 | try assumption]
end.

Ltac mKE := let i := fresh "i" in let H := fresh "H"
            in apply KE_lemma; intros i H; repeat destruct H.

Ltac mEK g := apply EK_lemma with g; [> | try minG].

Ltac mCE := match goal with
  | |- E' ?n ?g' (E ?g' ?p') _ => unfold E'; fold (E' (S n) g' p'); mCE
  | |- E' ?n ?g' ?p' _  => apply CE_lemma
  | |- E ?g' (E ?g' ?p') _ => fold (E' 1 g' p'); mCE
  | |- E ?g' ?p' _ => fold (E' 0 g' p'); apply CE_lemma
end.

Ltac mCK' g:= match goal with
  | |- K ?i' (K _ _) ?w' => mK_i; mCK' g; only 1 : mK_e i' w'; only 1 : mEK g
  | |- K ?i' ?p' _ => mEK g
end.

Ltac mCK g := mCK' g; only 1 : mCE.

Ltac mKD i := apply KD_lemma with i; [> try minG | ].


(* Modal accessibility axioms *)
Axiom Rreflexivity: forall i x, R i x x.
Axiom Rtransitivity: forall i x y z, (R i x y) -> (R i y z) -> (R i x z).
Axiom Rsymmetry: forall i x y, (R i x y) -> (R i y x).
Theorem Reuclidean: forall i x y z, (R i x y) -> (R i x z) -> (R i y z).
Proof.
intros.
cut (R i y x).
+ intro.
  apply Rtransitivity with (y := x).
  exact H1.
  exact H0.
+ apply Rsymmetry.
  exact H.
Qed.


(* Modal axioms *)
Lemma KT_lemma: forall (w:W) (i:A) (p:P), K i p w -> p w.
Proof.
intros w i p.
intro H.
apply (Ke_lemma i w).
exact H.
apply Rreflexivity.
Qed. 

Lemma K4_lemma: forall (w:W) (i:A) (p:P), K i p w -> K i (K i p) w.
Proof.
intros w i p H.
mK_i.
mK_i.
mK_e i w.
exact H.
apply Rtransitivity with (y := w0).
exact R0.
exact R1.
Qed.

Lemma K5_lemma: forall (w:W) (i:A) (p:P), (m~ K i p) w -> K i (m~ (K i p)) w.
Proof.
intros w i p H.
mK_i.
unfold mnot in *.
unfold not in *.
unfold K in *.
intro.
destruct H.
intros.
apply H0.
apply Reuclidean with (x := w).
exact R0.
exact H.
Qed.

Lemma RE'transitivity: forall n m g x y z, (RE' n g x y) -> (RE' m g y z) -> (RE' (1 + n + m) g x z).
Proof.
intros n m i x y.
induction m.
+ intros.
  simpl.
  unfold RE' in H0.
  exists y.
  split.
  rewrite <- plus_n_O.
  exact H.
  exact H0.
+ intros.
  simpl.
  simpl in H0.
  destruct H0.
  destruct H0.
  exists x0.
  split.
  * assert (n + S m = 1 + n + m).
    - simpl.
      rewrite <- plus_n_Sm.
      reflexivity.
    - rewrite H2.
      apply IHm.
      exact H.
      exact H0.
  * exact H1.
Qed.

Lemma RE'reverse: forall n g x y, (RE' (S n) g x y) -> (exists z, RE g x z /\ RE' n g z y).
Proof.
intros n g.
induction n.
+ intros.
  simpl in H.
  destruct H.
  destruct H.
  exists x0.
  split.
  exact H.
  exact H0.
+ intros.
  simpl in H.
  destruct H.
  destruct H.
  destruct H.
  destruct H.
  destruct IHn with x x0.
  * simpl.
    exists x1.
    split.
    exact H.
    exact H1.
  * destruct H2.
    exists x2.
    split.
    exact H2.
    simpl.
    exists x0.
    split.
    exact H3.
    exact H0.
Qed.

Lemma RE'symmetry: forall n g x y, (RE' n g x y) -> (RE' n g y x).
Proof.
intros n g.
induction n.
+ intros.
  simpl.
  unfold RE' in H.
  unfold RE.
  unfold RE in H.
  destruct H.
  destruct H.
  exists x0.
  split.
  exact H.
  apply Rsymmetry.
  exact H0.
+ intros.
  apply RE'reverse in H.
  destruct H.
  destruct H.
  simpl.
  exists x0.
  split.
  * apply IHn.
    exact H0.
  * unfold RE.
    unfold RE in H.
    destruct H.
    destruct H.
    exists x1.
    split.
    exact H.
    apply Rsymmetry.
    exact H1.
Qed.

Lemma RE'Euclidean: forall n m g x y z, (RE' n g x y) -> (RE' m g x z) -> (RE' (1 + n + m) g y z).
Proof.
intros n m g x y.
intros.
apply RE'transitivity with x.
+ apply RE'symmetry.
  exact H.
+ exact H0.
Qed.

Lemma RCtransitivity: forall g x y z, (RC g x y) -> (RC g y z) -> (RC g x z).
Proof.
intros g x y z H H0.
unfold RC in *.
destruct H.
destruct H0.
exists (1 + x0 + x1).
apply RE'transitivity with (y:=y).
exact H.
exact H0.
Qed.

Lemma RCEuclidean: forall g x y z, (RC g x y) -> (RC g x z) -> (RC g y z).
Proof.
intros g x y z H H0.
unfold RC in *.
destruct H.
destruct H0.
exists (1 + x0 + x1).
apply RE'Euclidean with (x:=x).
exact H.
exact H0.
Qed.

Lemma C4_lemma: forall (w:W) (g:G) (p:P), C g p w -> C g (C g p) w.
Proof.
intros w g p H.
mC_i.
mC_i.
apply (proj1 (RCC g w p)) with (y:=w1) in H .
exact H.
apply RCtransitivity with w0.
exact RC0.
exact RC1.
Qed.

Lemma C5_lemma: forall (w:W) (g:G) (p:P), (m~ (C g p)) w -> C g (m~ (C g p)) w.
Proof.
intros w g p H.
mC_i.
unfold mnot in *.
unfold not in *.
intro H0.
apply H.
apply RCC.
intros w1 RC1.
apply (proj1 (RCC g w0 p)).
exact H0.
apply RCEuclidean with (x:=w).
exact RC0.
exact RC1.
Qed.

Lemma DT_lemma: forall (w:W) (g:G) (p:P), D g p w -> p w.
Proof.
intros w g p H.
unfold D in H.
apply H.
intros.
apply Rreflexivity.
Qed.

Lemma D4_lemma: forall (w:W) (g:G) (p:P), D g p w -> D g (D g p) w.
Proof.
intros w g p H.
mD_i.
mD_i.
unfold RD in *.
unfold D in H.
apply H.
intros.
apply Rtransitivity with (y:=w0).
+ cut (g i).
  generalize i.
  exact RD0.
  exact H0.
+ cut (g i).
  generalize i.
  exact RD1.
  exact H0.
Qed.

Lemma D5_lemma: forall (w:W) (g:G) (p:P), (m~ (D g p)) w -> D g (m~ (D g p)) w.
Proof.
intros w g p H.
mD_i.
unfold mnot in *.
unfold RD in RD0.
unfold not in *.
unfold D in *.
intro H0.
apply H.
intros y H1.
apply H0.
intros.
apply Reuclidean with (x:=w).
+ cut (g i).
  generalize i.
  exact RD0.
  exact H2.
+ cut (g i).
  generalize i.
  exact H1.
  exact H2.
Qed.

Ltac mKT i := apply KT_lemma with i.
Ltac mK4 := apply K4_lemma.
Ltac mK5 := apply K5_lemma.
Ltac mC4 := apply C4_lemma.
Ltac mC5 := apply C5_lemma.
Ltac mDT g := apply DT_lemma with g.
Ltac mD4 := apply D4_lemma.
Ltac mD5 := apply D5_lemma.

Create HintDb modal.
Hint Unfold mimplies mnot mor mand K E C D mA mE Valid : modal.
