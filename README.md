# Multi Agent Systems (S5^n) in Coq
This is a Coq implementation of the modal logic S5^n, which I made for my bachelor thesis computing
science. The implementation is based on an S5 implementation by Bruno Woltzenlogel Paleo and
Christoph Benzmueller. Natural deduction rules, based on ProofWeb, are also included.

Made with CoqIDE version 8.11.0.

For more information, see [my thesis](https://www.cs.ru.nl/bachelors-theses/2021/Michiel_Philipse___1016359___Distributed_Knowledge_Proofs_in_the_Coq_Proof_Assistant.pdf).
